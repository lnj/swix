/*
 * SWIX - Service for Web Integration of XMPP
 * Copyright (C) 2020 Linus Jahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either vers<ion 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

use std::fs::File;
use std::io::Read;
use std::path::Path;

// rocket
use rocket::response::content::Html;
use rocket_contrib::json::{Json};

// xmpp
use futures::{future, Sink, Stream};
use std::convert::TryFrom;
use tokio::runtime::current_thread::Runtime;
use tokio_xmpp::{xmpp_codec::Packet, Client};
use xmpp_parsers::{
    disco::{DiscoInfoQuery, DiscoInfoResult},
    iq::{Iq, IqType},
    ns,
    server_info::ServerInfo,
    Element, Jid,
};

#[derive(Serialize, Deserialize)]
struct Config {
    jid: String,
    password: String
}

fn parse_cfg<P: AsRef<Path>>(path: P) -> Option<Config> {
    match File::open(path) {
        Ok(mut f) => {
            let mut input = String::new();
            match f.read_to_string(&mut input) {
                Ok(_) => match toml::from_str(&input) {
                    Ok(toml) => Some(toml),
                    Err(_) => None,
                },
                Err(_) => None,
            }
        }
        Err(_) => None,
    }
}

#[get("/")]
fn index() -> Html<&'static str> {
    Html("<html>Hello, world!</html>")
}

#[derive(Default, Serialize, Deserialize)]
struct ServerAddresses {
    abuse: Vec<String>,
    admin: Vec<String>,
    feedback: Vec<String>,
    sales: Vec<String>,
    security: Vec<String>,
    support: Vec<String>
}

impl From<ServerInfo> for ServerAddresses {
    fn from(info: ServerInfo) -> Self {
        ServerAddresses {
            abuse: info.abuse,
            admin: info.admin,
            feedback: info.feedback,
            sales: info.sales,
            security: info.security,
            support: info.support
        }
    }
}

#[get("/server/<jid>/addresses")]
fn addresses(jid: String) -> Option<Json<ServerAddresses>> {
    let cfg = match parse_cfg(dirs::config_dir().unwrap().join("swix.toml")) {
        Some(cfg) => cfg,
        None => {
            return None;
        }
    };
    
    let mut addresses: ServerAddresses = ServerAddresses::default();

    // tokio_core context
    let mut rt = Runtime::new().unwrap();
    // Client instance
    let client = Client::new(&cfg.jid, &cfg.password).unwrap();

    // Make the two interfaces for sending and receiving independent
    // of each other so we can move one into a closure.
    let (mut sink, stream) = client.split();

    // Wrap sink in Option so that we can take() it for the send(self)
    // to consume and return it back when ready.
    let mut send = move |packet| {
        sink.start_send(packet).expect("start_send");
    };

    // Main loop, processes events
    let mut wait_for_stream_end = false;
    let done = stream.for_each(|event| {
        if wait_for_stream_end {
            /* Do Nothing. */
        } else if event.is_online() {
            println!("Connected to XMPP!");

            let target_jid: Jid = jid.clone().parse().unwrap();
            let iq = make_disco_iq(target_jid);

            println!("Sending disco#info request to {}", jid.clone());
            println!(">> {}", String::from(&iq));

            send(Packet::Stanza(iq));
        } else if let Some(stanza) = event.into_stanza() {
            if stanza.is("iq", "jabber:client") {
                match Iq::try_from(stanza) {
                    Ok(iq) => {
                        if let IqType::Result(Some(payload)) = iq.payload {
                            if payload.is("query", ns::DISCO_INFO) {
                                if let Ok(disco_info) = DiscoInfoResult::try_from(payload) {
                                    for ext in disco_info.extensions {
                                        if let Ok(server_info) = ServerInfo::try_from(ext) {
                                            addresses = ServerAddresses::from(server_info);
                                            wait_for_stream_end = true;
                                            send(Packet::StreamEnd);
                                        }
                                    }
                                }
                            }
                        }
                    },
                    Err(_) => {
                        wait_for_stream_end = true;
                        send(Packet::StreamEnd);
                        ()
                    }
                };
            }
        }

        Box::new(future::ok(()))
    });

    // Start polling `done`
    match rt.block_on(done) {
        Ok(_) => (),
        Err(e) => {
            println!("Fatal: {}", e);
            return None;
        }
    }

    Some(Json(addresses))
}

fn make_disco_iq(target: Jid) -> Element {
    Iq::from_get("disco", DiscoInfoQuery { node: None })
        .with_id(String::from("contact"))
        .with_to(target)
        .into()
}

fn main() {
    rocket::ignite().mount("/", routes![index, addresses]).launch();
}
